// terdapat sebuah fungsi bernama body yang menerima dua parameter
// berupa haus dan lapar yang berupa boolean
// lengkapi fungsi tersebut dimana jika haus itu true maka akan return saya mau minum
// jika lapar itu true maka return saya mau makan
// jika lapar dan haus itu true maka return saya mau makan dan minum
// jika keduanya false maka return saya baik baik saja
// contoh:
// input -> body(true,false)
// output -> saya mau minum

function body(haus, lapar) {
  // tulis kodemu disini
}

body(true, false);
body(false, false);
body(true, true);
body(false, true);
