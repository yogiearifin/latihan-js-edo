// function test () {
//     console.log("this is a function")
// }

// test()

// const test2 = () => {
//     console.log("this is an arrow function")
// }

// test2()

// const print = (word) => {
//     console.log(`the parameter is ${word}`)
// }

// print("robot")

// const greet = (person) => {
//     console.log("hello, my name is " + person)
// }

// greet("yogie")

// function greet2 (person) {
//     console.log("hello, my name is " + person)
// }

// greet2("Edo")

// const profile = (name,age) => {
//     console.log(`Hello, my name is ${name} and I am ${age} years old`)
//     // console.log("Hello, my name is "+ name + " and I am " + age + " years old")

// }

// profile("Edo",32)

// const newName = (name) => {
//     return `My name is ${name}`
// }

// console.log(newName("Yogie"))

const isEven = (number) => {
    if(number % 2 === 0) {
        return "this is an even number"
    } else {
        return "this is not an even number"
    }
}

// console.log(isEven(3))
// console.log(isEven(4))

const isHot = (temprature) => {
    if(temprature >= 30) {
        return "the room is hot"
    } else {
        return "the room is not hot"
    }
}

console.log(isHot(26))
console.log(isHot(30))